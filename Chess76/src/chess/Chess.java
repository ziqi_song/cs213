package chess;

import board.Board;
import error.GameOver;
import error.IllegalInput;
import error.LevelUp;
import error.NoMove;
import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import piece.Piece;
import piece.Piece.Color;

/**
 * @ZiqiSong
 */
public class Chess {
    private static Color color = Color.WHITE;
    private static Board board = Board.getBoard();

    /**
     * convert position like e2 to x, y position
     * @param posStr
     * @return
     */
    public static int[] convertPos(String posStr) {
        int[] posArr = new int[2];
        posArr[0] = posStr.charAt(0) - 'a';
        posArr[1] = posStr.charAt(1) - '1';
        return posArr;
    }

    /**
     * if use one file as the param of the program, the file format need be like following
     * e2 e4
     * f2 g2
     * e4 f4 Draw
     * @param filePath
     */
    public static void loadFile(String filePath) {
        Piece.board = board;

        try (BufferedReader br = new BufferedReader(new FileReader(filePath))) {
            String line;
            while ((line = br.readLine()) != null) {
                if (line.isEmpty()) {
                    continue;
                }

                System.out.println(line);
                line = line.toLowerCase();
                if (line.equals("resign")) {
                    resign();
                    break;
                }

                String[] tmp = line.split(" ");
                if (tmp.length == 3 && tmp[2].startsWith("draw")) {
                    System.out.println("Draw");
                    break;
                }

                if (tmp.length == 3) {
                    LevelUp.upPiece = tmp[2];
                }
                int[] start = convertPos(tmp[0]);
                int[] end = convertPos(tmp[1]);
                if(oneMove(start[0], start[1], end[0], end[1])) {
                    break;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * if use two files as the param of the program, the first file format need be like following
     * e2
     * f2
     * the second file format need be
     * e4
     * g4 N
     * @param from from file
     * @param to to file
     */
    public static void loadFile(String from, String to) {
        Piece.board = board;

        List<String> fromList = new ArrayList<>();
        List<String> toList = new ArrayList<>();
        try (
                BufferedReader brFrom = new BufferedReader(new FileReader(from));
                BufferedReader brTo = new BufferedReader(new FileReader(to));
        ) {
            String line;
            while ((line = brFrom.readLine()) != null) {
                fromList.add(line.toLowerCase());
            }

            while ((line = brTo.readLine()) != null) {
                toList.add(line.toLowerCase());
            }

            for (int i = 0; i < fromList.size(); i++) {
                String fromStr = fromList.get(i);
                String toStr = toList.get(i);
                if (fromStr.contains("resign") || toStr.contains("resign")) {
                    resign();
                } else if (fromStr.contains("draw") || toStr.contains("draw")) {
                    System.out.println("Draw");
                    break;
                } else {
                    String[] tmp = toStr.split(" ");
                    int[] end;
                    end = convertPos(tmp[0]);
                    if (tmp.length == 2) {
                        LevelUp.upPiece = tmp[1];
                    }
                    int[] start = convertPos(fromStr);
                    if(oneMove(start[0], start[1], end[0], end[1])) {
                        break;
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * resign
     */
    private static void resign() {
        if (color == Color.WHITE) {
            System.out.println("Black wins");
        } else {
            System.out.println("White wins");
        }
    }

    /**
     * move one piece
     * @param x1
     * @param y1
     * @param x2
     * @param y2
     * @return
     */
    public static boolean oneMove(int x1, int y1, int x2, int y2) {
        Piece piece = board.getPiece(x1, y1);
        if (piece == null || piece.getColor() != color || !piece.movable()) {
            System.out.println("invalid choose, no piece or not movable");
            return false;
        }

        try {
            board.move(x1, y1, x2, y2, color);
        } catch (IllegalInput e) {
            System.out.println("illegal move");
            return true;
        } catch (GameOver e) {
            System.out.println(e.getWinColor() + " wins");
            return true;
        } catch (NoMove e) {
            System.out.println("No move");
            return true;
        }

        color = color == Color.WHITE ? Color.BLACK : Color.WHITE;

        if (color == Color.WHITE) {
            System.out.println("turn to white：");
        } else {
            System.out.println("turn to black：");
        }
        return false;
    }

    /**
     * no input file, so interact with console
     * @throws GameOver
     * @throws NoMove
     * @throws IllegalInput
     */
    public static void loadConsole() throws GameOver, NoMove, IllegalInput {
        Board board = Board.getBoard();
        Piece.board = board;
        board.print();

        Scanner scanner = new Scanner(System.in);
        int x1, x2, y1, y2;
        String str;
        System.out.println("turn to white");
        while (true) {
            System.out.print("please input piece position:");
            str = scanner.next();
            if (str.equals("resign")) {
                if (color == Color.WHITE) {
                    System.out.println("Black win");
                } else {
                    System.out.println("White win");
                }
                return;
            }
            x1 = (int) (str.charAt(0)) - (int) 'a';
            y1 = str.charAt(1) - '1';
            scanner.nextLine();

            System.out.print("please input target position:");
            str = scanner.next();
            x2 = (int) (str.charAt(0)) - (int) 'a';
            y2 = str.charAt(1) - '1';
            scanner.nextLine();

            if(oneMove(x1, y1, x2, y2)) {
                break;
            }
        }
    }

    public static void main(String[] args) throws GameOver, NoMove, IllegalInput {
        if (args.length == 0) {
            loadConsole();
        } else if (args.length == 1) {
            loadFile(args[0]);
        } else if (args.length == 2) {
            loadFile(args[0], args[1]);
        }
    }
}
