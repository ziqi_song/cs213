package piece;

/**
 * a rook piece
 * @ZiqiSong
 */
public class Rook extends Piece {

    /**
     * create rook
     * @param posX
     * @param posY
     * @param color
     */
    public Rook(int posX, int posY, Color color) {
        super(posX, posY, Type.ROOK, color);
    }

    @Override
    public boolean checkPosition(int targetX, int targetY, Piece existing) {
        if (targetX != posX && targetY != posY) {
            return false;
        }

        return board.pathEmpty(posX, posY, targetX, targetY);
    }

    @Override
    public boolean movable() {
        return check(posX + 1, posY) || check(posX, posY + 1)
                || check(posX - 1, posY) || check(posX, posY - 1);
    }


}
