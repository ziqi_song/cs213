package piece;

/**
 * a queen piece
 * @ZiqiSong
 */
public class Queen extends Piece {

    public Queen(int posX, int posY, Color color) {
        super(posX, posY, Type.QUEEN, color);
    }

    @Override
    public boolean checkPosition(int targetX, int targetY, Piece exisiting) {
        if ((targetX != posX && targetY != posY) && Math.abs(targetX - posX) != Math.abs(targetY - posY)) {
            return false;
        }

        return board.pathEmpty(posX, posY, targetX, targetY);
    }

    @Override
    public boolean movable() {
        return check(posX + 1, posY + 1) || check(posX - 1, posY + 1)
                || check(posX + 1, posY - 1) || check(posX - 1, posY - 1)
                || check(posX + 1, posY) || check(posX, posY + 1)
                || check(posX - 1, posY) || check(posX, posY - 1);
    }
}
