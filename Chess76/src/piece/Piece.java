package piece;

import board.Board;
import error.GameOver;
import error.IllegalInput;
import error.LevelUp;

/**
 * basic class for piece
 * @ZiqiSong
 */
public abstract class Piece {
    public static Board board;

    /**
     * piece color
     */
    public enum Color {
        WHITE("w"), BLACK("b");
        private String value;

        Color(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }
    }

    /**
     * piece type
     */
    public enum Type {
        KING("K"),
        KNIGHT("N"),
        BISHOP("B"),
        PAWN("p"),
        QUEEN("Q"),
        ROOK("R");

        private String value;

        Type(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }
    }

    protected int posX;
    protected int posY;
    protected Color color;
    protected Type type;
    // to be shown
    protected String symbol;
    protected boolean dead;
    protected boolean hasMoved;

    public Piece(int posX, int posY, Type type, Color color) {
        this.posX = posX;
        this.posY = posY;
        this.color = color;
        setType(type);
    }

    /**
     * check if pos x y is available
     * @param x
     * @param y
     * @return
     */
    public boolean check(int x, int y) {
        if (x < 0 || x > 7 || y < 0 || y > 7) {
            return false;
        }

        // if there is same color piece, return false
        Piece existing = board.getPiece(x, y);
        if (existing != null && existing.getColor() == color) {
            return false;
        }

        return checkPosition(x, y, existing);
    }


    /**
     * set piece position
     * @param posX
     * @param posY
     */
    public void setPos(int posX, int posY) {
        this.posX = posX;
        this.posY = posY;
    }

    /**
     * check
     * @param x
     * @param y
     * @return
     */
    public boolean control(int x,int y){
        return check(x,y);
    }

    /**
     * move from current position to another
     * @param toX
     * @param toY
     * @throws GameOver
     * @throws LevelUp
     * @throws IllegalInput
     */
    public void moveTo(int toX, int toY) throws GameOver, LevelUp, IllegalInput {
        if (check(toX, toY)) {
            leave();
            setPos(toX, toY);
            arrive();
            hasMoved = true;
        } else {
            throw new IllegalInput();
        }
    }

    /**
     * leave current position, set to null
     */
    protected void leave(){
        board.clearPiece(posX, posY);
    }

    /**
     * arrive at new position, eat piece if possible
     * @throws GameOver
     * @throws LevelUp
     */
    protected void arrive() throws GameOver, LevelUp {
        board.setPiece(this);
        Piece existing = board.getPiece(posX, posY);
        if (existing != this && existing != null) {
            existing.kill();
        }
    }

    /**
     * check detail
     * @param targetX
     * @param targetY
     * @param existing
     * @return
     */
    public abstract boolean checkPosition(int targetX, int targetY, Piece existing);

    /**
     * check if movable
     * @return
     */
    public abstract boolean movable();

    /**
     * kill piece
     * @throws GameOver
     */
    public void kill() throws GameOver {
        dead = true;
    }

    public boolean isDead() {
        return dead;
    }

    /**
     * set piece type
     * @param type
     */
    public void setType(Type type) {
        this.symbol = color.getValue() + type.getValue();
        this.type = type;
    }

    public int getPosX() {
        return posX;
    }

    public int getPosY() {
        return posY;
    }

    public Type getType() {
        return type;
    }

    public String getSymbol() {
        return symbol;
    }

    public Color getColor() {
        return color;
    }

    @Override
    public String toString() {
        return "Piece{" +
                "posX=" + posX +
                ", posY=" + posY +
                ", color=" + color +
                ", type=" + type +
                ", symbol='" + symbol + '\'' +
                ", dead=" + dead +
                ", hasMoved=" + hasMoved +
                '}';
    }
}
