package piece;

/**
 * a bishop piece
 * @ZiqiSong
 */
public class Bishop extends Piece {

    public Bishop(int posX, int posY, Color color) {
        super(posX, posY, Type.BISHOP, color);
    }

    /**
     * bishop pos check
     * @param targetX
     * @param targetY
     * @param existing
     * @return
     */
    @Override
    public boolean checkPosition(int targetX, int targetY, Piece existing) {
        return Math.abs(targetX - posX) == Math.abs(targetY - posY) && board.pathEmpty(posX, posY, targetX, targetY);
    }

    @Override
    public boolean movable() {
        return check(posX + 1, posY + 1) || check(posX - 1, posY + 1)
                || check(posX + 1, posY - 1) || check(posX - 1, posY - 1);
    }
}
