package piece;

import error.GameOver;
import error.IllegalInput;
import error.LevelUp;

/**
 * a pawn piece
 * @ZiqiSong
 */
public class Pawn extends Piece {
    private boolean passedBy;
    private int passByX;
    private int passByY;

    public Pawn(int posX, int posY, Color color) {
        super(posX, posY, Type.PAWN, color);

        passByX = -1;
        passByY = -1;
    }

    /**
     * check position and also check if passedBy possible
     * @param toX
     * @param toY
     * @param existing
     * @return
     */
    @Override
    public boolean checkPosition(int toX, int toY, Piece existing) {
        if ((color == Color.WHITE && toY <= posY) || (color == Color.BLACK && toY >= posY)) {
            return false;
        }

        if (!hasMoved && posX == toX &&
                ((color == color.WHITE && toY ==posY+2) || (color == color.BLACK && toY == posY-2))
                && board.getPiece(toX, toY) == null
                && ((color == color.WHITE && board.getPiece(posX,posY+1) == null)
                ||(color == color.BLACK && board.getPiece(posX,posY-1) == null)) ) {
            if (color == color.WHITE) {
                for (int i = 8; i < 16; i++) {
                    Pawn pawn = (Pawn) board.getPiece(i, Color.BLACK);
                    if (pawn.control(posX,posY+1)){
                        pawn.passedBy = true;
                        pawn.passByX = posX;
                        pawn.passByY = posY+1;
                    }
                }
                return true;
            } else {
                for (int i = 8; i < 16; i++) {
                    Pawn pawn = (Pawn) board.getPiece(i, Color.WHITE);
                    if (pawn.control(posX,posY-1)){
                        pawn.passedBy = true;
                        pawn.passByX = posX;
                        pawn.passByY = posY-1;
                    }
                }
                return true;
            }
        }

        if (existing != null) {
            if ((color == Color.WHITE && (toY - posY == 1 && Math.abs(toX - posX) == 1))
                    || (color == Color.BLACK && (toY - posY == -1 && Math.abs(toX - posX) == 1))) {
                return true;
            } else {
                return false;
            }
        } else {
            if (posX != toX) {
                return false;
            }

            if (!hasMoved) {
                if ((color == Color.WHITE && toY - posY > 2) || (color == Color.BLACK && posY - toY > 2)) {
                    return false;
                }

                if (color == Color.WHITE && toY - posY == 2 && board.getPiece(posX, posY + 1) != null) {
                    return false;
                }

                if (color == Color.BLACK && posY - toY == 2 && board.getPiece(posX, posY - 1) != null) {
                    return false;
                }
            } else {
                if ((color == Color.WHITE && toY - posY != 1) || (color == Color.BLACK && posY - toY != 1)) {
                    return false;
                }
            }
        }

        return true;
    }

    @Override
    public boolean control(int toX, int toY){
        if (color == Color.WHITE && toY == posY + 1 && (toX == posX + 1 || toX == posX - 1)) {
            return true;
        }
        if (color == Color.BLACK && toY == posY - 1 && (toX == toX + 1 || toX == posX - 1)) {
            return true;
        }

        return false;
    }


    /**
     * if passedBy, try kill
     * @param toX
     * @param toY
     * @throws GameOver
     * @throws LevelUp
     * @throws IllegalInput
     */
    @Override
    public void moveTo(int toX, int toY) throws GameOver, LevelUp, IllegalInput {
        if (passedBy) {
            if (toX == passByX && toY == passByY) {
                leave();
                setPos(toX, toY);
                arrive();

                Piece pawn = color == Color.WHITE ? board.getPiece(posX, posY - 1) : board.getPiece(posX, posY + 1);
                pawn.leave();
                System.out.println("pawn kill");
                pawn.kill();

                passedBy = false;
            }
        } else {
            super.moveTo(toX, toY);
        }
    }

    @Override
    public boolean movable() {
        if (color == Color.WHITE) {
            return check(posX - 1, posY + 1) || check(posX, posY + 1) || check(posX + 1, posY + 1);
        } else {
            return check(posX - 1, posY - 1) || check(posX, posY - 1) || check(posX + 1, posY - 1);
        }
    }

    @Override
    protected void arrive() throws GameOver, LevelUp {
        Piece existing = board.getPiece(posX, posY);
        if (existing != this && existing != null) {
            existing.kill();
        }

        if ((color == Color.WHITE && posY == 7) || (color == Color.BLACK && posY == 0)) {
            throw new LevelUp(posX, posY, color);
        }

        setPos(posX, posY);
    }
}
