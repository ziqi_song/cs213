package error;

import piece.Piece.Color;

/**
 * pawn level up
 * @ZiqiSong
 */
public class LevelUp extends Exception {
    // piece after upgrade
    public static String upPiece;
    // position
    public int x, y;
    // color
    public Color color;

    public LevelUp(int x, int y, Color color) {
        this.x = x;
        this.y = y;
        this.color = color;
    }
}
