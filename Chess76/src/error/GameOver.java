package error;

import piece.Piece.Color;

/**
 * game over error
 * @ZiqiSong
 */
public class GameOver extends Exception{
    // win color
    Color winColor;

    /**
     * constructor
     * @param winColor
     */
    public GameOver(Color winColor) {
        this.winColor = winColor;
    }

    /**
     * get color
     * @return
     */
    public Color getWinColor() {
        return winColor;
    }
}
